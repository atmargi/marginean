name := """roSilo"""
organization := "modex"

version := "1.0-SNAPSHOT"

lazy val myProject = (project in file("."))
  .enablePlugins(PlayJava,PlayEbean)

scalaVersion := "2.13.1"

libraryDependencies += jdbc
libraryDependencies += guice
libraryDependencies ++= Seq(evolutions,jdbc)
libraryDependencies += javaJdbc
libraryDependencies += "com.h2database" % "h2" % "1.4.192"



